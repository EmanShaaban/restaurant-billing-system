import Models._
import Helpers._

object Main extends App {

  val Cola = Item("Cola", "Cold", "Drink", Pence(50))
  val Coffee = Item("Coffee", "Hot", "Drink", Pound(1))
  val CheeseSandwich = Item("Cheese Sandwich", "Cold", "Food", Pound(2))
  val SteakSandwich = Item("Steak Sandwich", "Hot", "Food", Pound(4.50))

  val order = List(Cola, Coffee, CheeseSandwich)

  val amount = calcBill(order)
  println("Amount : " + amount + "£")

  val tips = calcService(amount, order)
  if (tips != 0) println(s"Tip : ${tips}£ \nTotal : ${amount + tips}£")


}


