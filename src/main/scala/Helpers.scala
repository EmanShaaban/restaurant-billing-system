import Models.Item

object Helpers {

  def calcBill(items: List[Item]): BigDecimal = {
    var sum = BigDecimal(0)
    items.foreach(i => sum += i.price.value)
    sum
  }

  def calcService(amount: BigDecimal, order: List[Item]) = {
    val food = order.filterNot(o => o.`type`.equals("Drink"))
    val hot = food.filter(f => f.state.equals("Hot"))

    var tips = BigDecimal(0)
    if (hot.length > 0) {
      tips = getTips(0.2)
    } else if (food.length > 0) {
      tips = getTips(0.1)
    }

    def getTips(percent: BigDecimal): BigDecimal = {
      val tip = amount * percent
      val res: BigDecimal = if (tip > 20) 20 else tip
      res
    }

    tips
  }
}
