
object Models {

  case class Item(name: String, state: String, `type`: String, price: Price)

  case class Menu(item: Item)

  trait Price {
    def value: BigDecimal
  }

  case class Pence(valuee: BigDecimal) extends Price {
    val value = valuee / 100
  }

  case class Pound(value: BigDecimal) extends Price

}
