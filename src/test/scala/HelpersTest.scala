import Models._
import org.scalatest._

class HelpersTest extends FunSuite with Matchers {

  val Cola = Item("Cola", "Cold", "Drink", Pence(50))
  val Coffee = Item("Coffee", "Hot", "Drink", Pound(1))
  val CheeseSandwich = Item("Cheese Sandwich", "Cold", "Food", Pound(2))
  val SteakSandwich = Item("Steak Sandwich", "Hot", "Food", Pound(4.50))

  test("Bill total of empty list should return 0") {
    Helpers.calcBill(List()) should be(0)
  }
  test("Bill total of Cola, Coffee should return 1.5") {
    Helpers.calcBill(List(Cola, Coffee)) should be(1.5)
  }

  test("Bill total of Cola, Coffee, CheeseSandwich should return 3.5") {
    Helpers.calcBill(List(Cola, Coffee, CheeseSandwich)) should be(3.5)
  }

  test("The service charge of Drinks only should return 0.") {
    Helpers.calcService(1.5, List(Cola, Coffee)) should be(0)
  }


  test("The service charge of order that contains Food.") {
    Helpers.calcService(3.5, List(Cola, Coffee, CheeseSandwich)) should be(0.35)
  }

  test("The service charge of order that contains Hot food.") {
    Helpers.calcService(8, List(Cola, Coffee, CheeseSandwich, SteakSandwich)) should be(1.6)
  }
}
